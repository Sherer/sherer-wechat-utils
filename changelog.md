## 0.0.6（2023-03-22）
- 新增 `getVoice` 方法，用于获取语音消息
- 新增 `getMpLink` 方法，用于获取小程序链接消息
- 修复 `getImage` 方法里的 `MsgType` 参数错误问题
## 0.0.5（2022-09-11）
- 修复 调用 `sendCustomMsg` 方法时出现的 `数据格式` 错误问题
## 0.0.4（2022-09-11）
- 修复 调用 `sendCustomMsg` 方法时出现的 `data` 以声明错误
## 0.0.3（2022-09-10）
- 新增 `sendCustomMsg` 方法，用于发送客服消息
## 0.0.2（2022-06-30）
- 修复 获取加密消息时计算签名里丢失加密文本错误；
## 0.0.1（2022-06-28）
- 新增 `isPlainObject`、`isFn`、`clone`、`decryptData`、`encrypt`、`md5`、`sha1`、`sha256`、`getSignStr`、`getNonceStr`、`isXml`、`toXml`、`toJson`、`urlToJson`、`jsonToUrl`、`dateFormat` 等常用函数；
- 新增 `getUserInfo`、`getOfficialUserInfo` 等获取微信用户信息接口；
- 新增 `WechatSalon` 微信公众号和微信小程序服务器配置类；
